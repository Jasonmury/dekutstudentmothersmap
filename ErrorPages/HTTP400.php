<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once('meta_tags.php'); ?>
  <title>Error 400 - Bad Request</title>
  </head>
<body>
  <?php require_once('body.php'); ?>
    <div class="cover">
        <h1>Bad Request <small>Error 400</small></h1>
        <p class="lead">The server cannot process the request due to something that is perceived to be a client error.</p>
    </div>
  <?php require_once('footer.php'); ?>
</html>
