<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once('meta_tags.php'); ?>
  <title>Error 403 - Access Denied</title>
</head>

<body>
  <?php require_once('body.php'); ?>
    <div class="cover">
        <h1>Access Denied <small>Error 403</small></h1>
        <p class="lead">The requested resource requires an authentication.</p>
    </div>
  <?php require_once('footer.php'); ?>
</html>
