<!DOCTYPE html>
<html lang="en">
<head>
  <?php require_once('meta_tags.php'); ?>
  <title>Error 404 - Resource not found</title>
</head>

<body>
    <?php require_once('body.php'); ?>
    <div class="cover">
        <h1>Resource not found <small>Error 404</small></h1>
        <p class="lead">The requested resource could not be found but may be available again in the future.</p>
    </div>
    <?php require_once('footer.php'); ?>
</html>
