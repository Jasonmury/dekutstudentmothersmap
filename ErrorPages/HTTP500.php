<!DOCTYPE html>
<html lang="en">
<head>
  <?php require_once('meta_tags.php'); ?>
    <title>Error 500 - Webservice currently unavailable</title>
</head>
<body>
    <?php require_once('body.php'); ?>
    <div class="cover">
        <h1>Webservice currently unavailable <small>Error 500</small></h1>
        <p class="lead">An unexpected condition was encountered.<br />
Our service team has been dispatched to bring it back online.</p>
    </div>
  <?php require_once('footer.php'); ?>
</html>
