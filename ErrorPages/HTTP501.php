<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once('meta_tags.php'); ?>
  <title>Error 501 - Not Implemented</title>
</head>

<body>
    <?php require_once('body.php'); ?>
    <div class="cover">
        <h1>Not Implemented <small>Error 501</small></h1>
        <p class="lead">The Webserver cannot recognize the request method.</p>
    </div>
  <?php require_once('footer.php'); ?>
</html>
