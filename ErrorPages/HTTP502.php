<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once('meta_tags.php'); ?>
    <title>Error 502 - Webservice currently unavailable</title>
</head>

<body>
    <?php require_once('body.php'); ?>
    <div class="cover">
        <h1>Webservice currently unavailable <small>Error 502</small></h1>
        <p class="lead">We've got some trouble with our backend upstream cluster.<br />
Our service team has been dispatched to bring it back online.</p>
    </div>
  <?php require_once('footer.php'); ?>
</html>
