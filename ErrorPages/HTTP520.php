<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once('meta_tags.php'); ?>
    <title>Error 520 - Origin Error - Unknown Host</title>
</head>

<body>
    <?php require_once('body.php'); ?>
    <div class="cover">
        <h1>Origin Error - Unknown Host <small>Error 520</small></h1>
        <p class="lead">The requested hostname is not routed. Use only hostnames to access resources.</p>
    </div>
  <?php require_once('footer.php'); ?>
</html>
