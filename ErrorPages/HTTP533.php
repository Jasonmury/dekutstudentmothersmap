<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once('meta_tags.php'); ?>
    <title>Error 533 - Scheduled Maintenance</title>
</head>

<body>
    <?php require_once('body.php'); ?>
    <div class="cover">
        <h1>Scheduled Maintenance <small>Error 533</small></h1>
        <p class="lead">This site is currently down for maintenance.<br />
Our service team is working hard to bring it back online soon.</p>
    </div>
  <?php require_once('footer.php'); ?>
</html>
