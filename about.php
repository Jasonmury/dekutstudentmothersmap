<div class="modal-dialog modal-lg" id="about-container">
<?php $u_id = $_POST['u_id']; ?>
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<span class="modal-title">Institutions and Individuals Who made This project a Success</span>
</div>
<div class="modal-body">
<div class="row">
<div class="col-sm-6 col-xs-12" id="about-dekut">
<span>Dedan Kimathi university of Technology</span>
</div>
<div class="col-sm-6 col-xs-12" id="about-iggres">
<span>Institute of Geomatics engineering, Geospatial information systems and Remote Sensing</span>
</div>
</div>
<div class="row">
<div class="col-sm-6 col-xs-12" id="about-dekut-vc">
<span>Prof. Kioni</span>
</div>
<div class="col-sm-6 col-xs-12" id="about-iggres-director">
  <span>Dr. Murimi Ngigi</span>
</div>
</div>
<div class="row">
<div class="col-sm-6 col-xs-12" id="about-app">
<span>DeKUT Student Mothers Graphical record App</span>
</div>
<div class="col-sm-6 col-xs-12" id="about-gdev">
<span>DeKUT Geo Developers</span>
</div>
</div>
<div class="row">
<div class="col-sm-6 col-xs-12" id="about-csk">
<span>Computer Society of Kimathi</span>
</div>
<div class="col-sm-6 col-xs-12" id="about-app-developers">
<span>Application Developers</span>
</div>
</div>
</div>
</div>
</div>
