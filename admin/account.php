
<div class="modal-dialog">
	<div id="container" class="register-student">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
				<form id="register-student">
									<h1>REGISTER <span>DeKUT STUDENT</span> IN SM APPLICATION</h1>
                  <div class="form">
                    <label for="eg-no">Student's University Registraton number</label>
												<input type="text" id="s-reg" placeholder="Registration Number." />
												<label for="s-reg">National Identification Number.</label>
												<input type="text"  id="s-id-no" placeholder="ID Number" />
                        <label for="s-reg">Student's Email Address.</label>
												<input type="email"  id="s-email" placeholder="email-address@example.com" />
											<div class="clear"></div>
										<button type="button"  id="register-student">REGISTER</button>
										<div id="register-admin-link">Register app Admin? <span id="register-admin">register here!</span></div>
										<span id="status"></span>
									</div>
									<div class="clear"></div>
								</form>
							</div>

	<div id="container" class="register-admin">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
        <form  id="register-admin">
            <fieldset>
							<div id="first_step">
									<h1>REGISTER <span>Admin </span>For <span>DeKUT</span> SM APP</h1>
									<div class="form">
												<input type="text"  id="admin-emp-no" value="admin-employee-number" />
												<label for="admin-emp-no">Recognised employee number* </label>
												<input type="text" id="admin-id-no" value="admin-ID-number" />
												<label for="admin_id">National ID Number* </label>
												<input type="text" id="admin-phone" value="admin-phone-number" />
												<label for="phone-no">Admin Phone Number(+254...)* </label>
										</div><div class="clear"></div>
										<button id="register_first_step" >next</button>
										<div id="register-student-link">Register Student Mother? <span id="register-student">register here!</span></div>
									</div> <div class="clear"></div>
						</fieldset>

						<fieldset>
							<div id="second_step">
								 <h1>REGISTER <span>Admin </span>For <span>DeKUT</span> SM APP</h1>
									<div class="form">
										<label for="email">Valid email address* </label>
											<input type="email"  id="admin-email" value="admin-email-address@example.com" />
											<label for="password">Full Name(first, middle & sur name order)* </label>
											<input type="text" id="admin-name" value="admin-full-name" />
									</div> <div class="clear"></div>
									<button class="previous" id="register-second-step">next</button>
									<button class="previous" id="prev_step_first">previous</button>

								</div><div class="clear"></div>
						</fieldset>


            <fieldset>
							<div id="final_step">
  								<h1>Confirm <span> Admin Details </span> For <span> DeKUT </span> SM APP</h1>
	                <div class="form">
	                    <h2>Administrator Signup Details.</h2>
	                    <table>
	                        <tr><td>Admin Name</td><td></td><td></td></tr>
	                        <tr><td>ID Number</td><td></td><td></td></tr>
	                        <tr><td>Employee Number</td><td></td><td></td></tr>
	                        <tr><td>Phone Number</td><td></td><td></td></tr>
	                        <tr><td>Email Address</td><td></td><td></td></tr>
												</table>
	                </div>
									   <div class="clear"></div>
										 <button id="register-admin-final">register</button>
										 <button class="previous" id="prev_step_second">previous</button>
									<span id="status"></span>
							</div><div class="clear"></div>
						</fieldset>

        </form>
	</div>
	<div id="progress_bar">
        <div id="progress"></div>
        <div id="progress_text">0% Complete</div>
	</div>
</div>
