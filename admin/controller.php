<?php
require_once('controller/db.php');
require_once('controller/userClass.php');
$formats = array("jpg", "png", "gif", "bmp", "jpeg", "PNG", "JPG", "JPEG", "GIF", "BMP", "tiff", "tif");

if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST"){
  $request = $_POST["request"];
  if($request == "register"){
   $name = $_FILES['file']['name'];
   $size = $_FILES['file']['size'];
   $tmp  = $_FILES['file']['tmp_name'];
   $imageType = $_FILES['file']['type'];
   $email = $_POST['email'];
   $username = $_POST['username'];
   $firstname = $_POST['firstname'];
   $lastname = $_POST['lastname'];
   $password = $_POST['password'];
   $cpassword = $_POST['cpassword'];
   $gender = $_POST["gender"];
   $country = $_POST['country'];

      $inputError = [];
      if(empty($username) || strlen(trim($username)) == 0){
      $inputError = "Username is required and cannot be Empty.";
      }
      else if(empty($password) || strlen(trim($password))==0 ||strlen(trim($password)) < 4 ){
          $inputError = "Strong Password of more than 4 Characters is required";
      }
      else if($password != $cpassword){
        $inputError = "Password confirmation doesn't match password.";
      }
      else if($size/1000 > 999 || $size/1000 < 50 ){
        $inputError = "Image File size must not be greater than 1Mb and not less than 50kb";
      }
      else{$newUser = new User( $username,$password,$email,$firstname,$lastname,$gender, $country, $tmp, $imageType);
      }
  }
  else if($request == "login"){
  session_start();
  $username = $_POST['username'];
  $password = $_POST['password'];
  if(!empty($username) && strlen(trim($username)) != 0){
    if(!empty($password) && strlen(trim($password)) > 5){
      global $file;
      global $server;
      global $table;
      $query = "SELECT u_name, u_id FROM $table WHERE u_name = :username OR u_email = :email AND u_password = :password";
    try{
      $search = $server->prepare($query);
      $search->execute(array(
        ':username'=>$username,
        ':email'=>$username,
        ':password'=>md5($password)
      ));
      $search->bindColumn(1, $Username);
      $search->bindColumn(2, $Id);
      $search->fetch(PDO::FETCH_BOUND);
      $registeredUsers = $search->rowCount();
    }
    catch(Exception $error){
      fwrite($file, "Login User System Malfunctioned.\n".$error->getMessage());
      die("Internal Application Errors. Try again Later.");
    }
        if($registeredUsers==1){
      $_SESSION['username'] = $Username;
      $getProfileImage = selectBlob($Id);
      if($getProfileImage){
        echo "index.php";
      }
      else{
        echo "Error Occured while trying to Retriver User Profile Image\nUser Id : ".$Id."\nUsername : ".$Username;
      }
            }else{
        echo "Username/password is incorrect.";
    }
  }else{
    echo "Password cannot be empty or less than 5 characters long!";
  }

    }
    else{
      echo "Username | Email is Required and therefore cannot be empty";
    }




    }
}

function selectBlob($id) {
  global $errLog;
  global $server;
  global $table;
      $sql = "SELECT u_profile_image, u_profile_image_type FROM $table  WHERE u_id = :id;";

      try{
        $stmt = $server->prepare($sql);
        $stmt->execute(array(":id" => $id));
        $stmt->bindColumn(1, $data, PDO::PARAM_LOB);
        $stmt->bindColumn(2, $mime);
        $stmt->fetch(PDO::FETCH_BOUND);
      }
      catch(Exception $error){
          fwrite($errLog, "Failed to Select User Profile Image \n".$error->getMessage());
      }
    if($data){
      $_SESSION['ProfileImage'] = $data;
      $_SESSION['imageMime'] = $mime;
      return true;
    }
    else{
      return false;
    }
}

fclose($errLog);

/*

 test insert gif image
$blobObj->insertBlob('images/JasonWeb.tif', "image/tif");
$a = $blobObj->selectBlob(7);
header("Content-Type:" . $a['mime']);
echo $a['data'];
 test insert pdf
$blobObj->insertBlob('pdf/php-mysql-blob.pdf',"application/pdf");
$a = $blobObj->selectBlob(7);
 save it to the pdf file
file_put_contents("pdf/output.pdf", $a['data']);
header("Content-Type:" . $a['mime']);
echo $a['data'];
 replace the PDF by gif file
$blobObj->updateBlob(3, 'images/Sheila-Lynn.jpg', "image/jpg");

$a = $blobObj->selectBlob();
header("Content-Type:" . $a['mime']);
echo $a['data'];
*/


?>
