<?php 
function storeSysLogs($error, $type){
    $date_time = date('D d M Y | H:i:s');
    file_put_contents("../logs/".$type.".file", $date_time." | ".$error."\n", FILE_APPEND | LOCK_EX);
    return true;
}
function queryResource($connect, $table, $sql){
    $prepareSQL = $connect->prepare($sql);
    $prepareSQL->execute();
     /*
  for($count = 0; $count < $prepareV->rowCount(); $count++){
    $result[$count]['p_img'] = "data:".$result[$count]['profile_type'].";base64,".base64_encode(selectUserProfile($result[$count]['regNo']));
  } */
    return $prepareSQL->fetchAll(PDO::FETCH_ASSOC);
}

?>