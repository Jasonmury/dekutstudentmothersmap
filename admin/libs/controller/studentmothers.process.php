<?php require_once('config.php');
if(isset($_POST['request']) && !empty($_POST['request'])){
$request = $_POST['request'];
if($request == 'visualize'){
  $connect = $GLOBALS['connect'];
  $table = $GLOBALS['table'];
  $sql = "SELECT regNo, userName, latitude, longitude, Description, profile_type FROM $table WHERE 1";
try{
  $prepareV = $connect->prepare($sql);
  $prepareV->execute();
  $result = $prepareV->fetchAll(PDO::FETCH_ASSOC);
  /*
  for($count = 0; $count < $prepareV->rowCount(); $count++){
    $result[$count]['p_img'] = "data:".$result[$count]['profile_type'].";base64,".base64_encode(selectUserProfile($result[$count]['regNo']));
  } */
  echo json_encode($result);
}
  catch(Exception $error){
  $log = $GLOBALS['log_file'];
 $file = fopen($log, 'a');
  fwrite($file, "Visualize data functionality Error.\n".$error->getMessage());
  fclose($file);
  echo "<span class='failed'>Sorry!.&nbsp;&rarr;&nbsp;Server Entered maintenance Mode</span>";
  }

}else if($request == "addStudentMother"){
    $s_name = $_POST['studentName'];
    $s_reg = $_POST['regNo'];
    $s_id_no = $_POST['id_no'];
    $lat = $_POST['latitude'];
    $lng = $_POST['longitude'];
    $s_detail = $_POST['detail'];
    /*
    $p_img = $_POST['file'];
    $f_size = $_POST['file_size'];
    $f_type = $_POST['file_type'];
    */
    $table = $GLOBALS['table'];
    $connect = $GLOBALS['connect'];
    $log = $GLOBALS['log_file'];
    $is_student = "SELECT national_id, reg_no FROM dekut_students WHERE national_id = :n_id AND reg_no = :reg";
    $is_registered = "SELECT regNo, national_id FROM $table WHERE regNo = :reg AND national_id = :n_id";
    $register = "INSERT INTO $table (regNo, national_id, userName, latitude, longitude, Description)
    VALUES (:reg, :n_id, :u_name, :lat, :lng, :detail)";
    try{
      $check_s = $connect->prepare($is_student);
      $check_s->execute(array(':n_id'=>$s_id_no, ':reg'=>$s_reg));
    }
    catch(Exception $error){
      $file = fopen($log, 'a');
      fwrite($file, "\nPrepare Statement error, Student registration status on dekut students relation failed.\n".$error->getMessage());
      fclose($file);
      die("<span class='failed'>Student registration Status Unknown.</span>");
    }
    if($check_s->rowCount() == 0){
      die("<span class='failed'>No DeKUT Student registered by provided registration number and ID number.
      This Service is usable by <i><b>Registered DeKUT Students Only.</i></b></br></span>");
    }else if($check_s != 1){
      $file = fopen($log, 'a');
      fwrite($file, "\nMultiple Student registration Incidence.\nStudent name: $s_name \nStudent Registration Number: $s_reg\nStudent National ID Number: $s_id_no.");
      fclose($file);
      die("<span class='failed'>Student registration Status Undefined.</br>Try Again Later.</span>");
    }
    try{
      $check_reg = $connect->prepare($is_registered);
      $check_reg->execute(array(':reg'=>$s_reg, ':n_id'=> $s_id_no));
    }catch(Exception $error){
      $file = fopen($log, 'a');
      fwrite($file, "\nPrepare Statement error, Student Mother registration status on student Mothers relation failed.\n".$error->getMessage());
      fclose($file);
      die("<span class='failed'>Student Mother registration Status Unknown.</span>");
    }

    if($check_reg->rowCount() != 0){
      die("<span class='failed'>Student With the above creditials is already Registered.</span>");
    }
    try{
      $reg_student = $connect->prepare($register);
      $reg_student->execute(array(':reg'=>$s_reg, ':n_id'=>$s_id_no, ':u_name'=>$s_name, ':lat'=>$lat, ':lng'=>$lng, ':detail'=>$s_detail));
    }
    catch(Exception $error){
      $file = fopen($log, 'a');
      fwrite($file, "\nStudent Mother Registration Failed.\n".$error->getMessage());
      fclose($file);
      die("<span class='failed'>Student Mother Registration Failed.</span>");
    }

    echo "<span class='success'>Student Mother Basic Registration Successful.</span>";
    updateStudentProfile($connect->lastInsertId(), $p_img, $f_type);
}
else if($request == 'profileImg'){
selectUserProfile($_POST['reg']);
}
else if($request == 'a-login'){
  $a_id = $_POST['admin_id'];
  $a_pass = $_POST['admin_pass'];
  $connect = $GLOBALS['connect'];
  $log = $GLOBALS['sys_log'];
  $does_exist = "SELECT admin_name FROM sys_admins WHERE emp_no = :u_id OR email = :u_id AND national_id = :n_id";
  try{
    $check = $connect->prepare($does_exist);
    $check->execute(array(':n_id'=>$a_pass, ':u_id'=>$a_id));
    if($check->rowCount() == 1){
      $result = $check->fetchAll(PDO::FETCH_ASSOC);
      session_start();
      $_SESSION['access'] = $result;
      echo 'admin/';
    }
    else if($check->rowCount() == 0){
      die("<span class='failed'>You don't have privilages to use this service.</span>");
    }
    else{
        $file = fopen($log, 'a');
        fwrite($file, "\nInconsistent Records Found.\nAdmin Employee Number/Email Address: $a_id\nId Number: $a_pass\nNumber of records retrived: ".$check->rowCount());
        fclose($file);
        die("<span class='failed'>You don't have privilages to use this service.</span>");
    }
  }
  catch(Exception $error){
    $file = fopen($log, 'a');
    $count = $check->rowCount();
    fwrite($file, "\nServer Unnresponsive to user loging.\nError Message: ".$error->getMessage());
    fclose($file);
    die("<span class='failed'>Sorry!. &nbsp;&rarr;&nbsp; Server is in maintenance Mode.</br><b>Try Again Later</b></span>");
  }
}
else if($request == 'registerStudent'){
  $s_id = $_POST['s_id'];
  $s_reg = $_POST['s_reg'];
  $s_email = $_POST['s_email'];
  $connect = $GLOBALS['connect'];
  $log = $GLOBALS['log_file'];
  $is_registered = "SELECT id FROM dekut_students WHERE reg_no = :s_reg AND national_id = :s_id";
  try{
    $check = $connect->prepare($is_registered);
    $check->execute(array(':s_reg'=>$s_reg, ':s_id'=>$s_id));
    if($check->rowCount() == 0){
      $registerSQL = "INSERT INTO dekut_students (reg_no, national_id, email) VALUES (:reg, :id, :email)";
      try{
        $register = $connect->prepare($registerSQL);
        $register->execute(array(':id'=>$s_id, ':email'=>$s_email, ':reg'=>$s_reg));
        if($register){
          echo true;
        }
        echo "<span class='failed'>Student Registration Failed.</span>";
      }
      catch(Exception $error){
        $file = fopen($log, 'a');
        fwrite($file, "DeKUT Student Registration Failed.\n".$error->getMessage());
        fclose($file);
        echo "<span class='failed'>Student Registration Failed.</span>";
      }
    }
    echo "<span class='failed'>Student is Already Registered...</span>";
  }
  catch(Exception $error){
    $file = fopen($log, 'a');
    fwrite($file, "DeKUT Student Registration Status Check Failed.\n".$error->getMessage());
    fclose($file);
    echo "<span class='failed'>Student Registration Failed.</span>";
  }
}

else if($request == 'registerAdmin'){
  $a_emp_no = $_POST['admin_emp_no'];
  $a_n_id = $_POST['admin_id'];
  $a_phone = $_POST['admin_phone'];
  $a_email = $_POST['admin_email'];
  $a_name = $_POST['full_name'];
  $connect = $GLOBALS['connect'];
  $log = $GLOBALS['log_file'];
  $a_exists = "SELECT id FROM sys_admins WHERE national_id = :n_id AND emp_no = :e_no OR email = :email";
  try{
    $check = $connect->prepare($a_exists);
    $check->execute(array(':email'=>$a_email, ':e_no'=>$a_emp_no, ':n_id'=>$a_n_id));
    if($check->rowCount() == 0){
      $reg_admin = "INSERT INTO sys_admins (emp_no, national_id, email, phone, admin_name)
      VALUES (:e_no, :a_id, :email, :phone, :a_name)";
      try{
        $register = $connect->prepare($reg_admin);
        $register->execute(array(':a_name'=>$a_name, ':email'=>$a_email, ':phone'=>$a_phone, ':a_id'=>$a_n_id, ':e_no'=>$a_emp_no));
        echo "<span class='success'>Administrator Registration Successful.</span>";
      }catch(Excption $error){
        $file = fopen($log, 'a');
        fwrite($file, "Administrator Registration Failed.\n".$error->getMessage());
        fclose($file);
        echo "<span class='failed'>Administrator Registration Failed.</span>";
      }
    }
    echo "<span class='failed'>Administrator is already Registered.</span>";
  }catch(Exception $error){
    $file = fopen($log, 'a');
    fwrite($file, "Administrator Registration Status Check Failed.\n".$error->getMessage());
    fclose($file);
    echo "<span class='failed'>Administrator Registration Failed.</span>";
  }
}



}
else{
  echo "<span class='failed'>Requested Service Not Supported.</span>";
}

function updateStudentProfile($u_id, $u_img, $type){
  $table = $GLOBALS['table'];
  $connect = $GLOBALS['connect'];
  $log = $GLOBALS['log_file'];
  $sql = "UPDATE $table SET profile = :data, profile_type= :mime WHERE Id = :id";
     try{
       $update = $connect->prepare($sql);
       $update->bindParam(':id', $u_id);
       $update->bindParam(':mime', $type);
       $update->bindParam(':data', $u_img, PDO::PARAM_LOB);
       $update->execute();
     }
     catch(Exception $error){
        $file = fopen($log, 'a');
        fwrite($file, "Failed to Update User profile picture \n".$error->getMessage());
        fclose($file);
        die("<span class='failed'>user Profile Image Update Failed.</span>");
       }
   echo "</br><span class='success'>user Profile Image Updated successfully.</span>";
}

   function selectUserProfile($u_reg){
         $table = $GLOBALS['table'];
         $server = $GLOBALS['connect'];
         $log = $GLOBALS['log_file'];
         $sql = "SELECT profile FROM $table  WHERE regNo = :u_reg";
         try{
           $result = $server->prepare($sql);
           $result->execute(array(":u_reg" => $u_reg));
           $result->bindColumn(1, $data, PDO::PARAM_LOB);
           $result->fetch(PDO::FETCH_BOUND);
         }
         catch(Exception $error){
           $file = fopen($log, 'a');
           fwrite($file, "Failed to Select User profile picture \n".$error->getMessage());
           fclose($file);
         }
         if($data){
           return $data;
         }else{
           return "Error";
         }
     }
?>
