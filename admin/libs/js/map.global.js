var smartNavbar = $('.navbar-custom');
smartNavbar.load('navbar.php', {'u_id': '1'});
var smartFooter = $('footer');
smartFooter.load('../footer.php');
var mapServices = $('#map-services');
mapServices.load('../mapservices.php');
var mapContent = $('#map-content .row');
mapContent.load('../mapContent.php');
$(window).load(function(){
  var contactsModal = $('#contacts-modal');
  var aboutModal = $('#about-modal');
  var registerModal = $('#register-modal');
$('#contacts').click(function(){
  contactsModal.load('../contacts.php',{'u_id': '1'},
  function(){
  contactsModal.modal('show');
  });
});
$('#about').click(function(){
  aboutModal.load('../about.php',{'u_id': '1'},
  function(){
  aboutModal.modal('show');
  });
});
$('#register').click(function(){
    registerModal.load('account.php', {}, function(){
      registerModal.modal('show');
      registerModal.on('shown.bs.modal', function(){
        loadModalEventListeners();
      });
      });

});
$('#logout').click(function(){

});


});


function loadModalEventListeners(){
  $('#container.register-student').show();
  $('#container.register-admin').hide();
  $('#progress_bar').hide();
  var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
  $('#register-admin-link span#register-admin').click(function(){
  $('#container.register-student').hide();
  $('#container.register-admin').show();
  $('#progress_bar').show();
  });

  $('#register-student-link span#register-student').click(function(){
  $('#container.register-student').show();
  $('#container.register-admin').hide();
  $('#progress_bar').hide();
  });

    var field_values = {
                'admin-emp-no'  : 'admin-employee-number',
                'admin-id-no'  : 'admin-ID-number',
                'admin-phone' : 'admin-phone-number',
                'admin-name'  : 'admin-full-name',
                'admin-email'  : 'admin-email-address@example.com',
                's-reg'    : 'Registration Number.',
                's-id-no'   : 'ID Number',
                's-email'   : 'email-address@example.comr'
        };

        $('form#register-admin input#admin-emp-no').inputfocus({ value: field_values['admin-emp-no'] });
        $('form#register-admin input#admin-id-no').inputfocus({ value: field_values['admin-id-no'] });
        $('form#register-admin input#admin-phone').inputfocus({ value: field_values['admin-phone'] });
        $('form#register-admin input#admin-name').inputfocus({ value: field_values['admin-name'] });
        $('form#register-admin input#admin-email').inputfocus({ value: field_values['admin-email'] });
        $('form#register-student input#s-reg').inputfocus({ value: field_values['s-reg'] });
        $('form#register-student input#s-id-no').inputfocus({ value: field_values['s-id-no'] });
        $('form#register-student input#s-email').inputfocus({ value: field_values['s-email'] });


        $('#progress').css('width','0');
        $('#progress_text').html('0% Complete');

        function nextContainer(current, next){
          current.find("*").fadeIn(0).slideUp();
          next.find("*").fadeOut().slideDown();
          next.nextAll('fildset').hide();
        }

      $('button#register_first_step').click(function(e){
        e.preventDefault();
        var phoneNumber = /[0-9-()+]{3,20}/;
        $('div#first_step .form input').removeClass('error').removeClass('valid');
            var fields = $('div#first_step .form input[type=text]');
            var error = 0;
            fields.each(function(){
                var value = $(this).val();
                if(value.length<4 || value==field_values[$(this).attr('id')] ||  $(this).attr('id')=='admin-phone' && !phoneNumber.test(value)) {
                    $(this).addClass('error');
                    error++;
                } else {
                    $(this).addClass('valid');
                }
            });
            if(error){
              bootbox.alert("<span class='text-danger failed error'>Input Fields Highlighted in Red border are Required and Cannot be Empty/Invalid</span>");
            }
              else if(!error) {
                    $('#progress_text').html('50% Complete');
                    $('#progress').css('width','170px');
                    var parentField = $(this).parents("fieldset");
                    var nextField = parentField.next();
                    nextContainer(parentField, nextField);
            }
             else return false;

        });

        $('button#register-second-step').click(function(e){
              e.preventDefault();
            $('#second_step .form input').removeClass('error').removeClass('valid');
            var fields = $('#second_step .form input[type=text], #second_step .form input[type=email]');
            var error = 0;
            fields.each(function(){
                var value = $(this).val();
                if( value.length<1 || value==field_values[$(this).attr('id')] || ( $(this).attr('type')=='email' && !emailPattern.test(value) ) ) {
                    $(this).addClass('error');
                    error++;
                } else {
                    $(this).addClass('valid');
                }
            });
            if(error){
              bootbox.alert("<span class='text-danger failed error'>Input Fields Highlighted in Red border are Required and Cannot be Empty/Invalid</span>");
            }
              else  if(!error) {
                  var fields = new Array(
                    $('#admin-name').val(),
                    $('#admin-id-no').val(),
                    $('#admin-emp-no').val(),
                    $('#admin-phone').val(),
                    $('#admin-email').val()
                 );
                   var tr = $('#final_step tr');
                   tr.each(function(){
                     $(this).children('td:nth-child(2)').html('&rarr;');
                     $(this).children('td:nth-child(3)').html(fields[$(this).index()]);
                   });
                  var parentField = $(this).parents("fieldset");
                  var nextField = parentField.next();
                  $('#progress_text').html('100% Complete');
                  $('#progress').css('width','339px');
                  nextContainer(parentField, nextField);
            }
            else return false;
            });

        $('button#register-admin-final').click(function(e){
        e.preventDefault();
    		var adminId = $('#admin-id-no').val();
    		var adminE_no = $('#admin-emp-no').val();
    		var adminEmail = $('#admin-email').val();
    		var adminPhone = $('#admin-phone').val();
    		var fullName = $('#admin-name').val();
        registerAdmin(adminId, adminE_no, adminEmail, adminPhone, fullName);
        return false;
          });

      $('.previous#prev_step_first, .previous#prev_step_second').click(function(e){
        e.preventDefault();
        var parentField = $(this).parents("fieldset");
        var prevField = parentField.prev();
        nextContainer(parentField, prevField);
        return false;
      });

      $("button#register-student").click(function(event){
        event.preventDefault();
        var student_reg ="";
        var student_id = "";
        var student_email = "";
        $( "input#s-reg" ).keyup(function() {
          student_reg = $( this ).val();
        }).keyup();
        $( "input#s-id-no" ).keyup(function() {
          student_id = $( this ).val();
        }).keyup();
        $( "input#s-email" ).keyup(function() {
          student_email = $( this ).val();
        }).keyup();
        if( $.trim(student_reg).length < 15){
        bootbox.alert("<span class='text-danger error failed'>student Registration Number Doesn't Match DeKUT Standards and pattern of Assignment.</span>");
        }
        else if($.trim(student_id).length != 8){
         bootbox.alert("<span class='text-danger error failed'>Invalid ID Number,  make necessary corrections and try again</span>");
       }else if($.trim(student_email).length < 9 || !emailPattern.test($.trim(student_email))){
          bootbox.alert("<span class='text-danger failed error'>Input value not a correct email address, make necessary corrections and try again</span>");
        }
        else{
          registerStudent(student_reg, student_id, student_email);
        }
        return false;
    });
}

function registerAdmin(adminId, adminE_no, adminEmail, adminPhone, fullName){
    var submitData = new XMLHttpRequest();
    var url = serverUrl;
    var request = 'registerAdmin';
    var vars = "admin_id="+adminId+"&admin_emp_no="+adminE_no+"&admin_phone="+adminPhone+
    "&admin_email="+adminEmail+"&full_name="+fullName+"&request="+request;
    submitData.open("POST", url, true);
    submitData.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    submitData.onreadystatechange = function() {
      if(submitData.readyState == 4 && submitData.status == 200) {
        var return_status = submitData.responseText;
          $('form#register-admin #status').html('<span id="success">'+return_status+'</span>');
        }
      }
      $('form#register-admin #status').html('<span id="success">Loading...</span>');
      submitData.send(vars);
      }

  function registerStudent(student_reg, student_id, student_email){
      var submitData = new XMLHttpRequest();
      var url = serverUrl;
      var request = 'registerStudent';
      var vars = "s_reg="+student_reg+"&s_id="+student_id+"&s_email="+student_email+"&request="+request;
      submitData.open("POST", url, true);
      submitData.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      submitData.onreadystatechange = function() {
        if(submitData.readyState == 4 && submitData.status == 200) {
            $('#register-student #status').html(submitData.responseText);
          }
        }
        $('#register-student #status').html('<span id="success">Loading...</span>');
        submitData.send(vars);
        }
