<div id="advanced-container" class="modal-dialog modal-md">
<?php $u_id = $_POST['u_id']; ?>
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<span class="modal-title">Select Advanced option to use</span>
</div>
<div class="modal-body">
  <div class="row">
    <div class="col-xs-6">
      <div id="advanced-options">
      <form id="advanced-form">
        <label>Fuzzy search
          <input id="fuzzy-search" type="radio" name="advanced" value="1"/>
        </label>

        <label>Traveller Assistant
          <input id="walk-help" type="radio" name="advanced" value="2"/>
        </label>
      </form>
      </div>
    </div>
    <div class="col-xs-6">
      <div id="advanced-options-doc">
    <p>
      Fuzzy search Gides a bunch of capabilities to search for services, locations, items in a wide area.
    </p>
    <p>
      Traveller Assistant Gives travelling statistics such as Distance, approximate driving time etc.
    </p>
      </div>
    </div>
  </div>
</div>
</div>
</div>
