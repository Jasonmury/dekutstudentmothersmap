<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="Dekut Student Mother graphical record, Map." />
    <meta name="author" content="Kinyua Jason Muriki" />
    <meta name="Email-address" content="jaysnmury@gmail.com" />
    <meta name="mobile-phone-contact" content="+254706959881" />
        <title>DeKUT STUDENT MOTHERS</title>
	<link rel="stylesheet" href="libs/css/bootstrap/css/bootstrap.css" />
  <link rel="stylesheet" href="libs/dist/css/map-icons.css" />
	<link rel="stylesheet" href="libs/css/map.style.css" />
	<link rel="stylesheet" href="libs/css/map_view.css"/>
  <link rel="stylesheet" href="libs/css/filtered.map.css" media="all" />
  <link href="img/vision2030.png" rel="icon">
</head>
<body>
<div class="navbar navbar-custom navbar-static-top" role="navigation"></div>
<div class="page-header">
    <h1>DEDAN KIMATHI UNIVSERSITY OF TECHNOLOGY STUDENTS MOTHERS 
    AND STUDENTS WITH DISABILITIES GRAPHICAL RECORD [MAP]</h1></h1>
</div>
<div id="contacts-modal" class="modal fade" tabindex="-2"></div>
<div id="about-modal" class="modal fade" tabindex="-2"></div>
<div id="login-modal" class="modal fade" tabindex="-2"></div>
<div id="advanced-modal" class="modal fade" tabindex="-2"></div>
<div id="map-services"></div>
<div id="map" ></div>
<div class="container-fluid" id="map-content">
<div class="row"></div>
</div>
<footer></footer>
</body>
<script src="libs/js/jQuery-2.1.4.min.js"></script>
<script type="text/javascript" src="libs/js/bootstrap.js"></script>
<script type="text/javascript" src="libs/js/map.global.js"></script>
<script type="text/javascript" src="libs/js/global.variables.js"></script>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBO-AHqXnLY2LySGh2EbgH80_s4MfUufoc&libraries=places,geometry,drawing"></script>
<script src="libs/dist/js/map-icons.js"></script>
<script src="libs/js/bootbox.js"></script>
<script type="text/javascript" src="libs/js/map.controller.js"></script>
<script type="text/javascript" src= "libs/js/map.view.js"></script>

</html>
