<?php require_once('config.php');
if(isset($_POST['request']) && !empty($_POST['request'])){
$request = $_POST['request'];
if($request == 'visualize'){
  $connect = $GLOBALS['connect'];
  $table = $GLOBALS['table'];
  $sql = "SELECT regNo, userName, latitude, longitude, Description FROM $table WHERE 1";
try{
  echo json_encode(queryResource($connect, $table, $sql));
}
  catch(Exception $error){
  $errorMsg = "Visualize data functionality Error | ".$error->getMessage();
  storeSysLogs($errorMsg, "error");
  echo "<div class='error'>Request To View saved Entries Failed. </br> Try again later.</div>";
  }

}

else if(request == "error"){
  storeSysLogs($_POST["error"], "request.response.error");
}

else if($request == "addStudentMother"){
    $s_name = $_POST['studentName'];
    $s_reg = $_POST['regNo'];
    $s_id_no = $_POST['id_no'];
    $lat = $_POST['latitude'];
    $lng = $_POST['longitude'];
    $s_detail = $_POST['detail'];
    /*
    $p_img = $_POST['file'];
    $f_size = $_POST['file_size'];
    $f_type = $_POST['file_type'];
    */
    $table = $GLOBALS['table'];
    $connect = $GLOBALS['connect'];
    $is_student = "SELECT national_id, reg_no FROM dekut_students WHERE national_id = :n_id AND reg_no = :reg";
    $is_registered = "SELECT regNo, national_id FROM $table WHERE regNo = :reg AND national_id = :n_id";
    $register = "INSERT INTO $table (regNo, national_id, userName, latitude, longitude, Description)
    VALUES (:reg, :n_id, :u_name, :lat, :lng, :detail)";
    try{
      $check_s = $connect->prepare($is_student);
      $check_s->execute(array(':n_id'=>$s_id_no, ':reg'=>$s_reg));
    }
    catch(Exception $error){
     $errorMsg = "Prepare Statement error, Student registration status on dekut students relation failed.\n".$error->getMessage();
     storeSysLogs($errorMsg, "debug");
      die("<div class='failed'>Student registration Status Unknown.</div>");
    }
    if($check_s->rowCount() == 0){
      die("<div class='failed'>No DeKUT Student registered by provided registration number and ID number.
      This Service is usable by <i><b>Registered DeKUT Students Only.</div>");
    }else if($check_s != 1){
      $errorMsg = "Multiple Student registration Incidence.\nStudent name: $s_name \nStudent Registration Number: $s_reg\nStudent National ID Number: $s_id_no.";
      die("<div class='failed'>Student registration Status Undefined.</br>Try Again Later.</div>");
    }
    try{
      $check_reg = $connect->prepare($is_registered);
      $check_reg->execute(array(':reg'=>$s_reg, ':n_id'=> $s_id_no));
    }catch(Exception $error){
      $errorMsg = "Prepare Statement error, Student Mother registration status on student Mothers relation failed.\n".$error->getMessage();
      storeSysLogs($errorMsg, "debug");
      die("<div class='failed'>Student Mother registration Status Unknown.</div>");
    }

    if($check_reg->rowCount() != 0){
      die("<div class='failed'>Student With the above creditials is already Registered.</div>");
    }
    try{
      $reg_student = $connect->prepare($register);
      $reg_student->execute(array(':reg'=>$s_reg, ':n_id'=>$s_id_no, ':u_name'=>$s_name, ':lat'=>$lat, ':lng'=>$lng, ':detail'=>$s_detail));
    }
    catch(Exception $error){
      $errorMsg = "Student Mother Registration Failed | ".$error->getMessage();
      storeSysLogs($errorMsg, 'debug');
      die("<div class='failed'>Student Mother Registration Failed Temp.</div>".$error->getMessage());
    }

    echo "success";
    //updateStudentProfile($connect->lastInsertId(), $p_img, $f_type);
}


else if($request == 'profileImg'){
selectUserProfile($_POST['reg']);
}


else if($request == 'a-login'){
  $a_id = $_POST['admin_id'];
  $a_pass = $_POST['admin_pass'];
  $connect = $GLOBALS['connect'];
  $log = $GLOBALS['sys_log'];
  $does_exist = "SELECT admin_name FROM sys_admins WHERE emp_no = :u_id OR email = :u_id AND national_id = :n_id";
  try{
    $check = $connect->prepare($does_exist);
    $check->execute(array(':n_id'=>$a_pass, ':u_id'=>$a_id));
    if($check->rowCount() == 1){
      $result = $check->fetchAll(PDO::FETCH_ASSOC);
      session_start();
      $_SESSION['access'] = $result;
      echo 'admin/';
    }
    else if($check->rowCount() == 0){
      storeSysLogs("Unauthorised Admin Login Attemp | User ID -> ".$a_id." Used passcode -> ".$a_pass, "attack.warning");
      die("<div class='failed'>You don't have privilages to use this service.</div>");
    }
    else{
        $errorMsg = "Inconsistent Records Found.\nAdmin Employee Number/Email Address: $a_id\nId Number: $a_pass\nNumber of records retrived: ".$check->rowCount();
        storeSysLogs($errorMsg, "records.indoubt");
        die("<div class='failed'>You don't have privilages to use this service.</div>");
    }
  }
  catch(Exception $error){
    $count = $check->rowCount();
    $errorMsg = "Server Unnresponsive to user loging.\nError Message: ".$error->getMessage();
    storeSysLogs($errorMsg, "error");
    die("<div class='error'>Sorry! Server Entered maintenance Mode.</br><b>Try Again Later</b></div>");
  }
}



}
else{
  echo "<div class='failed'>Requested Service Not Supported.</div>";
}

function updateStudentProfile($u_id, $u_img, $type){
  $table = $GLOBALS['table'];
  $connect = $GLOBALS['connect'];
  $log = $GLOBALS['log_file'];
  $sql = "UPDATE $table SET profile = :data, profile_type= :mime WHERE Id = :id";
     try{
       $update = $connect->prepare($sql);
       $update->bindParam(':id', $u_id);
       $update->bindParam(':mime', $type);
       $update->bindParam(':data', $u_img, PDO::PARAM_LOB);
       $update->execute();
     }
     catch(Exception $error){
        $file = fopen($log, 'a');
        fwrite($file, "Failed to Update User profile picture \n".$error->getMessage());
        fclose($file);
        die("<span class='failed'>user Profile Image Update Failed.</span>");
       }
   echo "</br><span class='success'>user Profile Image Updated successfully.</span>";
}

   function selectUserProfile($u_reg){
         $table = $GLOBALS['table'];
         $server = $GLOBALS['connect'];
         $log = $GLOBALS['log_file'];
         $sql = "SELECT profile FROM $table  WHERE regNo = :u_reg";
         try{
           $result = $server->prepare($sql);
           $result->execute(array(":u_reg" => $u_reg));
           $result->bindColumn(1, $data, PDO::PARAM_LOB);
           $result->fetch(PDO::FETCH_BOUND);
         }
         catch(Exception $error){
           $file = fopen($log, 'a');
           fwrite($file, "Failed to Select User profile picture \n".$error->getMessage());
           fclose($file);
         }
         if($data){
           return $data;
         }else{
           return "Error";
         }
     }
?>
