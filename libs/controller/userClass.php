<?php
class User{
    public $username;
    public $password;
    public $email;
    public $fname;
    public $lname;
    public $gender;
    public $country;
    public $profileImage;
    public $filePath = "exceptionLogs.txt";
    public $imageType;
    public function __construct($user, $pass, $email, $fname, $lname, $gender, $country, $profile, $type){
        $this->username = $user;
        $this->password = $pass;
        $this->email = $email;
        $this->fname= $fname;
        $this->lname = $lname;
        $this->gender = $gender;
        $this->country = $country;
        $this->profileImage = $profile;
        $this->imageType = $type;
        self::registerNewUser($this);
    }

    public function _setOnTheFly($propName){
        $this->$propName = $propValue;
    }
    public function _getOnTheFly($propName, $propValue){
        $objProperties = $this.get_object_vars();
        if(in_array($propName, $objProperties)){
            return $this->$propName;
        }
        else{
            return "Non Existing property";
        }

    }

    private function insertBlob($filePath) {
      global $errLog;
      global $server;
      global $table;
         $blob = fopen($filePath, 'rb');

         $sql = "INSERT INTO $table (u_profile_image) VALUES (:data)";
         try{
           $stmt = $server->prepare($sql);
           $stmt->bindParam(':data', $blob, PDO::PARAM_LOB);
         }
         catch(Exception $error){
           fwrite($errLog, "Failed to Insert User Profile Picture. \n".$error->getMessage());
         }
         return $stmt->execute();
     }

    private  function updateBlob($id, $filePath) {
      global $errLog;
      global $server;
      global $table;
         $blob = fopen($filePath, 'rb');
         $sql = "UPDATE $table SET `u_profile_image` = :data WHERE u_id = :id;";
       try{
         $stmt = $server->prepare($sql);
         $stmt->bindParam(':data', $blob, PDO::PARAM_LOB);
         $stmt->bindParam(':id', $id);
       }
       catch(Exception $error){
       fwrite($errLog, "Failed to Update User profile picture \n".$error->getMessage());
       }
         return $stmt->execute();
     }

     private  function updateBlobMime($id, $filePath, $mime) {
       global $errLog;
       global $server;
       global $table;
          $blob = fopen($filePath, 'rb');
          $sql = "UPDATE $table SET `u_profile_image` = :data, `u_profile_image_type` = :mime WHERE u_id = :id;";
        try{
          $stmt = $server->prepare($sql);
          $stmt->bindParam(':mime', $mime);
          $stmt->bindParam(':data', $blob, PDO::PARAM_LOB);
          $stmt->bindParam(':id', $id);
        }
        catch(Exception $error){
        fwrite($errLog, "Failed to Update User profile picture \n".$error->getMessage());
        }
          return $stmt->execute();
      }

   private function selectBlob($id) {
     global $errLog;
     global $server;
     global $table;
         $sql = "SELECT u_profile_image FROM $table  WHERE id = :id;";

         try{
           $stmt = $server->prepare($sql);
           $stmt->execute(array(":id" => $id));
           $stmt->bindColumn(1, $data, PDO::PARAM_LOB);
           $stmt->fetch(PDO::FETCH_BOUND);
         }
         catch(Exception $error){
             fwrite($errLog, "Failed to Select User Profile Image \n".$error->getMessage());
         }

         return $data;
     }

    private function registerNewUser($user){
       //echo "New Object registered\n".$this->fname." ".$this->lname;
        global $file;
        global $server;
        global $table;
        $select = "SELECT u_id, u_name, u_email FROM $table WHERE u_name = ? OR u_email = ?";
        $insert = "INSERT INTO $table (`u_id`, `u_name`, `u_email`, `u_first_name`, `u_last_name`, `u_password`, `u_country`, `u_gender`,`u_profile_image_type`, `time_stamp`) VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, CURRENT_TIMESTAMP)";
        try{
            $prepareSelect = $server->prepare($select);
            $prepareSelect->execute(array($user->username, $user->email));
            $prepareSelect->setFetchMode(PDO::FETCH_ASSOC);
            $fetchData =  $prepareSelect->fetchAll(PDO::FETCH_ASSOC);
            $rowCount = $prepareSelect->rowCount();
            $userId = $fetchData[0]["u_id"];
        }
        catch(Exception $error){
            fwrite($file, "Failed to Select Data From database \n".$error->getMessage());
            die("Internal Application Errors. Try again Later.");
        }
       if(!$rowCount){
            try{

            $prepare = $server->prepare($insert);
            $prepare->execute(array($user->username, $user->email, $user->fname, $user->lname, md5($user->password), $user->country, $user->gender, $user->imageType));
            $userId = $server->lastInsertId();
        }
        catch(Exception $error){
            fwrite($file, "Failed to insert Data into database \n".$error->getMessage());
            die("Internal Application Errors. Try again Later.");
        }
        if($prepare){
            $updateBlob = self::updateBlob($userId, $user->profileImage);
            if($updateBlob){
              echo "User Registered Successfully!";
            }
            else echo "Failed to insert User profile picture. \n Try again later.";
        }
    }
        else{
          $updateBlob = self::updateBlobMime($userId, $user->profileImage, $user->imageType);
          if($updateBlob){
              echo "another User by that username Or Email address is already registered. \nNumber of Registered users -> ".$rowCount;
          }
          else echo "Error encountered While trying to Update User profile Picture. \n Try again Later";
    }

  }

}

 ?>
