
		  function customizePolyline(){
				google.maps.event.addListener(drawingManager, 'polylinecomplete', function(polyline){
				var latlng = polyline.getPath().getArray().toString();
				var getlength = polyline.getLength();
				console.log('Shape Line Length : '+getlength/1000+'Km.\nLatLng String : '+latlng);
				});
			}

			function customizePolygon(){
				google.maps.event.addListener(drawingManager, 'polygoncomplete', function(polygon){
					var latlng = polygon.getPath().getArray().toString();
					var area = polygon.getArea();
					var perimeter = polygon.getLength();
					console.log('Shape Area : '+area/1000000+'Km sqd.\nShape Perimeter : '+perimeter/1000+'Km.\nLatLng String : '+latlng);
				});
			}
			function customizeCircle(){
				google.maps.event.addListener(drawingManager, 'circlecomplete', function(circle){
				var radius = circle.getRadius();
				console.log('Radius : '+radius);
				});
			}
			function customizeRectangle(){
				google.maps.event.addListener(drawingManager, 'rectanglecomplete', function(polygon){
				var bounds = polygon.getBounds().toString();
				console.log(bounds);
				});
			}


			google.maps.Polyline.prototype.getLength = function(){
			return google.maps.geometry.spherical.computeLength(this.getPath());
			};

			google.maps.Polygon.prototype.getArea = function(){
			return google.maps.geometry.spherical.computeArea(this.getPath());
			};
			google.maps.Polygon.prototype.getLength = function(){
			return google.maps.geometry.spherical.computeLength(this.getPath());
			};

			google.maps.Rectangle.prototype.getArea = function()
			{
			return google.maps.geometry.spherical.computeArea(this.getPath());
			};
			google.maps.Rectangle.prototype.getLength = function(){
			return google.maps.geometry.spherical.computeLength(this.getPath());
			};

			google.maps.Circle.prototype.getLength = function(){
			return google.maps.geometry.spherical.computeLength(this.getPath());
			};
			google.maps.Circle.prototype.getArea = function()
			{
			return google.maps.geometry.spherical.computeArea(this.getPath());
			};


		function geoLocator(div, map){
				div.style.clear = 'both';
				var locator = document.createElement('div');
				locator.id = 'locator';
				locator.title = 'Go To Your Current Location';
				div.appendChild(locator);
				var locatorTxt = document.createElement('div');
				locatorTxt.id = 'locatorTxt';
				locatorTxt.innerHTML = 'Geolocate';
				locator.appendChild(locatorTxt);
				locator.addEventListener('click', function(){
					if(navigator.geolocation){
						navigator.geolocation.getCurrentPosition(function(position){
							var lat = position.coords.latitude;
							var lng = position.coords.longitude;
							var divCenter = new google.maps.LatLng(lat, lng);
							map.setCenter(divCenter);
							map.setZoom(17);
							//console.log(lat+", "+lng);
							var marker = new google.maps.Marker({
								position: divCenter,
								title: "Your Current Location",
								clickable: true,
								draggable: false
							});
							marker.setMap(map);
							google.maps.event.addListener(marker, 'click', function(){
								var markerInfo = new google.maps.InfoWindow({
									content: "Your Current GPS Location was Determined as Follows.</br>( Latitude: "+
									lat+", Longitude: "+lng+")</br>Note: Location details are prone to position error of up to 300m"
								});
							});
							markerInfo.open(map, marker);
						});

					}
				});
			}

			function autocompleteCreate(div, map) {
		  var testlUI = document.createElement('div');
		  testlUI.title = 'Fill Form Filds Appropritely';
		  testlUI.id = 'autocompleteDiv';
			div.id = 'autocompleteUI';
			div.className = 'autocompleteUI';
		  div.appendChild(testlUI);
		 var autoFilds = document.createElement('div');
		 autoFilds.id = 'autoFilds';
		 var selectOptions = '<span> <select id="country">'+
		 				'<option value="ke">Kenya</option>'+
			   '<option value="tz">Tanzania</option>'+
			   '<option value="ug">Uganda</option>'+
				 '<option value="eth">Ethiopia</option>'+
			   '<option value="ssd">South Sudan</oprion>'+
			   '<option value="rw">Rwanda</option>'+
			  ' <option value="br">Burundi</option>'+
			   '<option value="cg">Congo</option>'+
			   '<option value="ngr">Nigeria</option>'+
				 '<option value="sa">South Africa</option>'+
				 '<option value="all">World</option>'+
		      '</select></span>';
		  var inputField =  '<span id="locationField"><input id="autocomplete" placeholder="Enter City" type="text" title="Enter name of city where to query services." /> </span>';
		  var labelText = '<span id="findhotels">Find <input type="text" id="service-search" title="Name of Service to search, eg church, bank, restaurant etc..."placeholder="eg... school"/> Filter </span>';
		  autoFilds.innerHTML = labelText + selectOptions + inputField;
		  testlUI.appendChild(autoFilds);
		  testlUI.addEventListener('click', function() {
			var autoInput = document.getElementById('autocomplete');
			addAutocomplete(map, autoInput);
		  });
		}
			function addAutocomplete(map, field){
		        autocomplete = new google.maps.places.Autocomplete(
		            (field), {
		              types: ['(cities)'],
		              componentRestrictions: countryRestrict
		            });
		        places = new google.maps.places.PlacesService(map);

		        autocomplete.addListener('place_changed', onPlaceChanged);

		        document.getElementById('country').addEventListener(
		            'change', setAutocompleteCountry);

			}

					 function onPlaceChanged() {
						var place = autocomplete.getPlace();
						var placeLocation = place.geometry.location;
						if (placeLocation) {
						  map.panTo(placeLocation);
						  map.setZoom(15);
						  search();
						} else {
						  document.getElementById('autocomplete').placeholder = 'Enter a city';
						}
					  }


						function search() {
						var service = $('#service-search').val();
						var search = {
						  bounds: map.getBounds(),
						  types: [service]
						};
						places.nearbySearch(search, function(results, status) {
						  if (status === google.maps.places.PlacesServiceStatus.OK) {
							clearResults();
							clearMarkers();

							for (var i = 0; i < results.length; i++) {
							  var markerLetter = String.fromCharCode('A'.charCodeAt(0) + (i % 26));
							  var markerIcon = MARKER_PATH + markerLetter + '.png';
							  markers[i] = new google.maps.Marker({
								position: results[i].geometry.location,
								animation: google.maps.Animation.DROP,
								icon: markerIcon
							  });

							  markers[i].placeResult = results[i];
							  google.maps.event.addListener(markers[i], 'click', showInfoWindow);
							  setTimeout(dropMarker(i), i * 100);
							  addResult(results[i], i);
							}
						  }
						});
					}
					  function clearMarkers() {
						for (var i = 0; i < markers.length; i++) {
						  if (markers[i]) {
							markers[i].setMap(null);
						  }
						}
						markers = [];
					  }

					  function setAutocompleteCountry() {
						var country = document.getElementById('country').value;
						if (country == 'all') {
						  autocomplete.setComponentRestrictions([]);
						  map.setCenter({lat: 15, lng: 0});
						  map.setZoom(2);
						} else {
						  autocomplete.setComponentRestrictions({'country': country});
						  map.setCenter(countries[country].center);
						  map.setZoom(countries[country].zoom);
						}
						clearResults();
						clearMarkers();
					  }

					  function dropMarker(i) {
						return function() {
						  markers[i].setMap(map);
						};
					  }
					  function addResult(result, i) {
						var results = document.getElementById('results');
						var markerLetter = String.fromCharCode('A'.charCodeAt(0) + (i % 26));
						var markerIcon = alphMarkerPath + markerLetter + '.png';
						var tr = document.createElement('tr');
						tr.style.backgroundColor = (i % 2 === 0 ? '#F0F0F0' : '#FFFFFF');
						tr.onclick = function() {
						  google.maps.event.trigger(markers[i], 'click');
						};
						var iconTd = document.createElement('td');
						var nameTd = document.createElement('td');
						var icon = document.createElement('img');
						icon.src = markerIcon;
						icon.setAttribute('class', 'placeIcon');
						icon.setAttribute('className', 'placeIcon');
						var name = document.createTextNode(result.name);
						iconTd.appendChild(icon);
						nameTd.appendChild(name);
						tr.appendChild(iconTd);
						tr.appendChild(nameTd);
						$('#close-result').css("display","block");
						results.appendChild(tr);
					  }

					  function clearResults() {
						var results = document.getElementById('results');
						while (results.childNodes[0]) {
						  results.removeChild(results.childNodes[0]);
						}
					  }

					  function showInfoWindow() {
						var marker = this;
						places.getDetails({placeId: marker.placeResult.place_id},
							function(place, status) {
							  if (status !== google.maps.places.PlacesServiceStatus.OK) {
								return;
							  }
							  infoWindow.open(map, marker);
							  buildIWContent(place);
							});
					  }

			  function buildIWContent(place) {
				document.getElementById('iw-icon').innerHTML = '<img class="hotelIcon" ' +
					'src="' + place.icon + '"/>';
				document.getElementById('iw-url').innerHTML = '<b><a href="' + place.url +
					'">' + place.name + '</a></b>';
				document.getElementById('iw-address').textContent = place.vicinity;

				if (place.formatted_phone_number) {
				  document.getElementById('iw-phone-row').style.display = '';
				  document.getElementById('iw-phone').textContent =
					  place.formatted_phone_number;
				} else {
				  document.getElementById('iw-phone-row').style.display = 'none';
				}

				if (place.rating) {
				  var ratingHtml = '';
				  for (var i = 0; i < 5; i++) {
					if (place.rating < (i + 0.5)) {
					  ratingHtml += '&#10025;';
					} else {
					  ratingHtml += '&#10029;';
					}
				  document.getElementById('iw-rating-row').style.display = '';
				  document.getElementById('iw-rating').innerHTML = ratingHtml;
				  }
				} else {
				  document.getElementById('iw-rating-row').style.display = 'none';
				}

				if (place.website) {
				  var fullUrl = place.website;
				  var website = hostRegexp.exec(place.website);
				  if (website === null) {
					website = 'http://' + place.website + '/';
					fullUrl = website;
				  }
				  document.getElementById('iw-website-row').style.display = '';
				  document.getElementById('iw-website').textContent = website;
				} else {
				  document.getElementById('iw-website-row').style.display = 'none';
				}
			  }

		  function displayRoute(service, display) {
			var totalDistEl = document.getElementById('dst-panel');
			 totalDistEl.removeAttribute('class', 'hideDiv');
			var wayptsArray = [];
			var wayptsLen = waypts.length  - 1;
			for(var i = 1; i<(wayptsLen); i++){
			  wayptsArray.push({location: {lat:waypts[i][0], lng:waypts[i][1]}});
			}
	        service.route({
	          origin: new google.maps.LatLng(waypts[0][0], waypts[0][1]),
	          destination: new google.maps.LatLng(waypts[wayptsLen][0], waypts[wayptsLen][1]),
	          waypoints: wayptsArray,
			  optimizeWaypoints: true,
	          travelMode: 'DRIVING',
	          avoidTolls: true
	        }, function(response, status) {
	          if (status === 'OK') {
	            display.setDirections(response);
				 var route = response.routes[0];
	          } else {
	            alert('Could not display directions due to: ' + status);
	          }
	        });
	      }

		   function displayNewRoute(wayPoint, service, display) {
			  var totalDistEl = document.getElementById('dst-panel');
			  totalDistEl.removeAttribute('class', 'hideDiv');
			  if(origin == null && destination == null){
				alert("You need to specify Start and End Points");
			}
	        service.route({
	          origin: origin,
	          destination: destination,
	          waypoints: wayPoint,
			  optimizeWaypoints: true,
	          travelMode: 'DRIVING',
	          avoidTolls: false
	        }, function(response, status) {
	          if (status === 'OK') {
	            display.setDirections(response);
				 var route = response.routes[0];
	          } else {
	            alert('Could not display directions due to: ' + status);
	          }
	        });
	      }

		  function displayMyRoute(service, display) {
			var totalDistEl = document.getElementById('dst-panel');
			totalDistEl.removeAttribute('class', 'hideDiv');
	        if(origin == null && destination == null){
				alert("You need to specify Start and End Points");
			}
			service.route({
	          origin: origin,
	          destination: destination,
	          travelMode: 'DRIVING',
	          avoidTolls: false
	        },
		function(response, status) {
	        if (status === 'OK') {
	          display.setDirections(response);
			 var route = response.routes[0];
	        } else {
	          alert('Could not display directions due to: ' + status);
	        }
	      });
	    }

	  function computeTotalDistance(result){
	    var total = 0;
	    var myroute = result.routes[0];
	    for (var i = 0; i < myroute.legs.length; i++) {
	      total += myroute.legs[i].distance.value;
	    }
	    total = total / 1000;
			document.getElementById('total').innerHTML = total + ' km';
	  }


		function advancedToolLayer(container, map){
			var advanced = document.createElement('div');
			advanced.id = 'advanced-tools';
			advanced.title = 'Switch To Expert Mode';
			container.appendChild(advanced);
			var icon = document.createElement('img');
			icon.id = 'advanced-tool-img';
			icon.alt = 'TOOLS';
			icon.src = 'libs/img/settings.jpeg';
			advanced.appendChild(icon);
			icon.addEventListener('click', function(){
           var advancedModal = $('#advanced-modal');
					 advancedModal.load('advanced-mode.php',{'u_id': '1'},
				   function(){
				   advancedModal.modal('show');
				   });
					 advancedModal.on('shown.bs.modal', function(){
						 $('form#advanced-form').on('change', function(){
							 if($('input[name=advanced]:checked', 'form#advanced-form').val() == 1){
								 if(!fuzzy_search){enableFuzzySearch(map);
									 $('#floating-panel').fadeOut();
									$('#autocompleteDiv').fadeIn();
									 fuzzy_search = 1;
								 }
								 $('#floating-panel').fadeOut();
							 	 $('#autocompleteDiv').fadeIn();
							 }else if($('input[name=advanced]:checked', 'form#advanced-form').val() == 2){
								 $('#floating-panel').css({
									 'display':'inline-block',
									 'visibility':'visible'
								 });
								$('#floating-panel').fadeIn();
							 $('#autocompleteDiv').fadeOut();
							 }
						 });
					 });
			});
		}

		function enableFuzzySearch(map){
			if($(document).outerWidth() > 1117){
				 var autocompleteElement = document.createElement('div');
				 var addAutocomplete = new autocompleteCreate(autocompleteElement, map);
				 autocompleteElement.index = 1;
				 map.controls[google.maps.ControlPosition.TOP_RIGHT].push(autocompleteElement);
			 }
		}


		function addlayersIcon(container, map){
			var iconDiv = document.createElement('div');
			iconDiv.id = 'iconDiv';
			iconDiv.title = 'Change BaseMap';
			container.appendChild(iconDiv);
			var icon = document.createElement('img');
			icon.id = 'layersImage';
			icon.alt = 'BaseMaps';
			icon.src = 'libs/img/Layers.png';
			iconDiv.appendChild(icon);
			var baseMapsHtml =
			'<div id="baseMapControl">'+
			'<input type="checkbox" id="satellite" /><label >Satellite</label><br />'+
			'<input type="checkbox" id="hybrid" /><label >Hybrid</label><br />'+
			'<input type="checkbox" id="terrain" /><label >Terrain</label><br />'+
			'<input type="checkbox" id="osm" /><label >OSM</label><br />'+
			'<input type="checkbox" id="bluestyle" /><label >BlueStyle</label><br />'+
			'<input type="checkbox" id="roadmap" /><label >RoadMap</label><br />'+
			'<input type="checkbox" id="nightMode" /><label >Night_Mode</label><br />'+
			'<input type="checkbox" id="africaLayer" /><label >Africa_Layer</label><br />'+
			'<input type="checkbox" id="kenyaRoads" /><label >Kenya_Roads</label><br />'+
			'<input type="checkbox" id="kenyaTowns" /><label >Kenya_Towns</label><br />'+
			'</div>';
			var layerDiv = document.createElement('div');
			layerDiv.id = 'basemaps';
			layerDiv.innerHTML = baseMapsHtml;
			icon.addEventListener('mouseover', function(){
			iconDiv.appendChild(layerDiv);
			$('#layersImage').css('margin-left','57px');
			$('#satellite, #hybrid, #terrain, #osm, #bluestyle,#roadmap, #nightMode').on('click', function(){
			$(this).toggleClass('checked');

					if($('#satellite').hasClass('checked')){
						map.setOptions({
							mapTypeId : google.maps.MapTypeId.SATELLITE
							});
					}
					else if($('#hybrid').hasClass('checked')){
						map.setOptions({
							mapTypeId: google.maps.MapTypeId.HYBRID
						});
					}
					else if($('#terrain').hasClass('checked')){
						map.setOptions({
							mapTypeId: google.maps.MapTypeId.TERRAIN
						});
					}
					else if($('#roadmap').hasClass('checked')){
						map.setOptions({
							mapTypeId: google.maps.MapTypeId.ROADMAP
						});
					}
					else if($('#osm').hasClass('checked')){
						map.setMapTypeId('OSM');
					}
					else if($('#bluestyle').hasClass('checked')){
					map.setMapTypeId('blue_baseMap');
					}
					else if($('#nightMode').hasClass('checked')){
					map.setMapTypeId('nightMode');
					}

			});


			$('#africaLayer').click(function(){
			$(this).toggleClass('checked');
			if($(this).hasClass('checked')){
				$.getJSON('libs/data/africa.geojson', function(africaLayer){
				$.each(africaLayer.features, function(key, val){
					drawLayer(val.geometry, val.properties, map);
				});
				});
			}
			});

			$('#kenyaRoads').click(function(){
				$(this).toggleClass('checked');
				if($(this).hasClass('checked')){
					$.getJSON('libs/data/kenya_roads.geojson', function(kenyaRoads){
					$.each(kenyaRoads.features, function(key, val){
						drawLayer(val.geometry, val.properties, map);
					});
					});
				}
			});

			$('#kenyaTowns').click(function(){
				$(this).toggleClass('checked');
				if($(this).hasClass('checked')){
					$.getJSON('libs/data/kenya_towns.geojson', function(kenyaTowns){
					$.each(kenyaTowns.features, function(key, val){
						drawLayer(val.geometry, val.properties, map);
					});
					});
				}
			});



			});


			icon.addEventListener('mouseout', function(){
				var timer = setTimeout(
				function(){
				iconDiv.removeChild(layerDiv);
				}
				, 5000);

			});
		}



		function getOverCooord(box, map){
			var coordDiv = document.createElement('div');
			coordDiv.id = 'overCoord';
			coordDiv.title = 'MouseOver Coordinates';
			google.maps.event.addListener(map, 'mousemove', function(e){
				var lat = e.latLng.lat().toFixed(5);
				var lng =e.latLng.lng().toFixed(5);
				coordDiv.innerHTML = '<h4><b><i>Lat , Lng &rarr; </i>\&nbsp;&nbsp;&nbsp;['+lat+' , ' +lng+'\]</b></h4>';
				box.appendChild(coordDiv);
			});
		}


		function drawLayer(geometry, properties){
				if(geometry.type == 'Point'){
					var coord = new google.maps.LatLng(geometry.coordinates[1], geometry.coordinates[0]);
					var marker = new google.maps.Marker({
						position: coord,
						map: map,
						title: 'Geometry marker'
					});
					var infoPop= new google.maps.InfoWindow({
						content: '<span id="feature-info">Town Name : '+properties.TOWN_NAME+'<br />Town Type : '+properties.TOWN_TYPE+'</span>'
					});
					google.maps.event.addDomListener(marker, 'click', function(){
						infoPop.open(map, marker);
					});
				}
				else if(geometry.type == 'LineString'){
					var linePath = [];
					for(var count = 0; count <geometry.coordinates.length; count++){
						var tmpPoints = new google.maps.LatLng(geometry.coordinates[count][1], geometry.coordinates[count][0]);
						linePath.push(tmpPoints);
					}
					var lineOptions = {
						path: linePath,
						strokeWeight: 3,
						strokeColor: 'red',
						map: map,
						title: "Kenya LineStrings"
					};
					var polyLine = new google.maps.Polyline(lineOptions);
					polyLine.setMap(map);
				}
				else if(geometry.type == 'MultiPolygon'){
					//console.log(properties);
					var areaCover = [];
					for (var count = 0; count < geometry.coordinates[0][0].length; count++){
						var tmpPoints = new google.maps.LatLng(geometry.coordinates[0][0][count][1], geometry.coordinates[0][0][count][0]);
						areaCover.push(tmpPoints);
					}
					var polygonOptions = {
						path: areaCover,
						strokeColor: "red",
						strokeOpacity: 0.7,
						strokeWeight: 2,
						fillColor: "violet",
						fillOpacity: 0.45,
						map: map
					};
					var polygon = new google.maps.Polygon(polygonOptions);
					polygon.setMap(map);
				}
			}

		function addCustomMarker(lat, lng, map, marker){
		var form = '<form id="form" action="" method="post" enctype="multipart/form-data"><input type="text" placeholder="Full Name*" id="name" name="name" /> <br /><br />' +
			'<span id="reg-no"><input type="text" name="regNo" id="reg" placeholder="Registration No / Email Address" title="Registration No / Email Address"/></span>'+
			'<span id="n-id"><input type="text" name="id_no" id="id_no" placeholder="National ID Number"/> </span>'+
			'<span id="u-desc"><textarea name="description" id="desc" type="text" placeholder="Give your brief Description, your location, '+
			'Challenges you face as a student mother and Form of assistance you may require." cols="40" rows="7"></textarea></span>' +
			'<span><button name="" class="btn btn-primary btn-sm" type="submit" id="btnSave" >SAVE STUDENT MOTHER</button></span></form/>'+
			'<div id="status"></div>';

		var infowindow = new google.maps.InfoWindow({
			content: form,
			maxWidth: 242
		});
		infowindow.open(map, marker);
		var fileName = 0, fileType = 0, fileSize = 0, fileSource = 0;
		$('input[type=file]').on('change', function(){
			var length = this.files.length;
			if(length == 1){
				var file = this.files[0];
				fileName = file.name;
				if(!file.type.match('image.*')){
					alert("Only Image files are allowed.");
					return false;
				}else{
					fileType = file.type;
				}
				if(file.size/1024000 >= 0.9999 ||  file.size/1024000 <= 0.05){
					alert("profile Picture Must be bigger than 50Kb and less than 1Mb in size.");
					return false;
				}else{
					fileSize = file.size;
				}
				if (window.FileReader) {
					  var reader = new FileReader();
					  reader.onloadend = function(e){
					   var source = e.target.result;
						 if(source){
							 fileSource = source;
						 }else{
							 alert("Profile Image Could not be prepared for uploading.");
							 return false;
						 }
					 };
					  reader.readAsDataURL(file);
					}
			}else{
				alert("Only One file can be uploaded.");
				return false;
			}
		});
		$('#btnSave').click(function(event){
			event.preventDefault();var studentName = $('#name').val();
			var regNumber = $('#reg').val();
			var specs = $('#desc').val();
			var idNo = $('#id_no').val();
			//var image = $('#profile').val();
		//	console.log(lat, lng, studentName, regNumber, specs, idNo);
		ajaxSubmit(lat, lng, studentName, regNumber, specs, idNo);
		});
	}

		function ajaxSubmit(lat, lng, name, regNumber, specs, id_no){
				if($.trim(name) == 0 || $.trim(regNumber) == 0 || $.trim(specs) == 0 || $.trim(id_no) == 0)
				{
				bootbox.alert("<span class='failed'>Please Fill All Fields</span>");
				}
				else
				{
				var submitData = new XMLHttpRequest();
				var url = serverUrl;
				var request = 'addStudentMother';
				var vars = "latitude="+lat+"&longitude="+lng+"&studentName="+name+
				"&detail="+specs+"&regNo="+regNumber+"&request="+request+'&id_no='+id_no;
				submitData.open("POST", url, true);
				submitData.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
				submitData.onreadystatechange = function() {
				if(submitData.readyState == 4 && submitData.status == 200) {
					if(submitData.responseText == "success"){
						$('#status').html('<span id="success">Reg Successful.</span>');
						bootbox.alert("<div class='success'>Student Mother Registration Successful</div>");
					}
						else{
							$('#status').html('<span id="success">Reg Failed.</span>');
							bootbox.alert(submitData.responseText);
						}
					}
				}
				$('#status').html('<span id="success">Loading...</span>');
				submitData.send(vars);
				}
			}
		function loadSavedRecords(map){
		var markerResp = [];
		var submitRequest = new XMLHttpRequest();
		var url = serverUrl;
		var request = 'visualize';
		var vars = "request="+request;
		submitRequest.open("POST", url, true);
		submitRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		submitRequest.onload = function() {
			if (this.status == 200) {
				console.log(this.response);
				try{
					$.each(JSON.parse(this.response), function(key, value){
						markerResp.push(value);
					});
					showMarkers(map, markerResp);
				}catch(e){
					$.ajax({
						data: {"error": e, "request": "error"},
						url: serverUrl,
						dataType: "json",
						error: function(status){bootbox.alert("Error Status: "+status.responseText);},
						success: function(){}
					});
					bootbox.alert("<div class='error'>"+this.response+"</div>");
				}
				
			}
		};
		submitRequest.send(vars);
		}

		function setStudentMarker(studentMother){
			var studentMarker = new Marker({
				position: new google.maps.LatLng(parseFloat(studentMother.latitude), parseFloat(studentMother.longitude)),
				icon: {
					path: MAP_PIN,
					fillColor: 'chocolate',
					fillOpacity: 0.6,
					strokeColor: 'chartreuse',
					strokeWeight: 0.7
				},
				map_icon_label: '<span class="map-icon map-icon-female"></span>',
				draggable: false,
				visible: true,
				/*place: {
					location: new google.maps.LatLng(parseFloat(studentMother.latitude), parseFloat(studentMother.longitude)),
					query: studentMother.userName+", "+studentMother.regNo+", "+studentMother.Description
				}, */
				animation: google.maps.Animation.DROP,
			});
			return studentMarker;
		}

		function setStudentInfo(studentMother){
			var info = '<div id="user-info"><span id="user-name">'+studentMother.userName+'</span>'+
				//'<img src="'+studentMother.p_img+'" alt="Profile Pic" title="'+studentMother.userName+'" id="profile-img" class="img-responsive img-rounded"/>'+
				'<span id="user-reg">Registration Number: '+studentMother.regNo+'</span><span id="user-specs">'+
				'Description: '+studentMother.Description+'</span><span id="user-location">Student Location: ['+
				studentMother.latitude+', '+studentMother.longitude+'].</span></div>';
			return info;
		}


		function showMarkers(map, studentMothers){
			infowindow = new google.maps.InfoWindow({
				content: "Static Content",
				maxWidth: 192,
			});
			var count = 0;
			while(count < studentMothers.length){
				var  marker = setStudentMarker(studentMothers[count]);
				marker.text = setStudentInfo(studentMothers[count]);
				console.log(studentMothers[count]);
				marker.setMap(map);
				google.maps.event.addListener(marker, 'click', function(){
					infowindow.setContent(this.text);
					infowindow.open(map, this);
				});
				count += 1;
			}
		}

		function searchBoxAdd(searchBox, map){
					map.addListener('bounds_changed', function() {
	          searchBox.setBounds(map.getBounds());
	        });
	        var markers = [];
	        searchBox.addListener('places_changed', function() {
	          var places = searchBox.getPlaces();
	          if (places.length == 0) {
	            return;
	          }
	          markers.forEach(function(marker) {
	            marker.setMap(null);
	          });
	          markers = [];
	          var bounds = new google.maps.LatLngBounds();
	          places.forEach(function(place) {
	            if (!place.geometry) {
	              console.log("Returned place contains no geometry");
	              return;
	            }
	            var icon = {
	              url: place.icon,
	              size: new google.maps.Size(71, 71),
	              origin: new google.maps.Point(0, 0),
	              anchor: new google.maps.Point(17, 34),
	              scaledSize: new google.maps.Size(25, 25)
	            };
	            markers.push(new google.maps.Marker({
	              map: map,
	              icon: icon,
	              title: place.name,
	              position: place.geometry.location
	            }));
	            if (place.geometry.viewport) {
	              bounds.union(place.geometry.viewport);
	            } else {
	              bounds.extend(place.geometry.location);
	            }
	          });
	          map.fitBounds(bounds);
	        });
		}
