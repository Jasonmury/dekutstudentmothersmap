var smartNavbar = $('.navbar-custom');
smartNavbar.load('navbar.php', {'u_id': '1'});
var smartFooter = $('footer');
smartFooter.load('footer.php');
var mapServices = $('#map-services');
mapServices.load('mapservices.php');
var mapContent = $('#map-content .row');
mapContent.load('mapContent.php');
$(window).load(function(){
  var contactsModal = $('#contacts-modal');
  var aboutModal = $('#about-modal');
  var loginModal = $('#login-modal');
$('#contacts').click(function(){
  contactsModal.load('contacts.php',{'u_id': '1'},
  function(){
  contactsModal.modal('show');
  });
});
$('#about').click(function(){
  aboutModal.load('about.php',{'u_id': '1'},
  function(){
  aboutModal.modal('show');
  });
});
$('#login').click(function(){
  loginModal.load('login.php', {}, function(){
    loginModal.modal('show');
  });
  loginModal.on('shown.bs.modal', function(){
    loginEventListeners();
  });
});

$('#close-result').click(function(e){
  e.preventDefault();
  if($(this).hasClass('show')){
    $('table#results-table').css('display', 'block');
    $(this).removeClass('show').html('hide contents');

  }else{
    $('table#results-table').css('display', 'none');
    $('#close-result').addClass('show').html('Show contents');
  }

});





});

function loginEventListeners(){
  $('#admin-login').click(function(event){
    var admin_id = "";
    var admin_pass = "";
    $( "input#login-username" ).keyup(function() {
      admin_id  = $( this ).val();
    }).keyup();
    $( "input#login-password" ).keyup(function() {
      admin_pass  = $( this ).val();
    }).keyup();
    if($.trim(admin_id).length < 12){
      bootbox.alert('<span class="failed">Employee Number or Email Address must be 12 Characters or more.'+admin_id+'</span>');
    }
    else if($.trim(admin_pass).length < 8){
    bootbox.alert('<span class="failed">Administrator Password must be 8 Characters Long.</span>');
  }else{
    $('#password-status, #username-status').html("");
    $('#status').html('<span class="success">Student Registration In Progress...</span>');
    loginSysAdmin(admin_id, admin_pass);

  }
  return false;
  });
}

function loginSysAdmin(a_id, a_pass){
  var submitData = new XMLHttpRequest();
  var url = serverUrl;
  var request = 'a-login';
  var vars = "admin_id="+a_id+"&admin_pass="+a_pass+"&request="+request;
  submitData.open("POST", url, true);
  submitData.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  submitData.onreadystatechange = function() {
    if(submitData.readyState == 4 && submitData.status == 200) {
        if(submitData.responseText.match('admin/')){
          $('#status').html('<span id="success">Login Successful.</span>');
          window.location = submitData.responseText;
        }else{
          bootbox.alert(submitData.responseText);
        }

      }
    }
    $('#status').html('<span id="success">Loading...</span>');
    submitData.send(vars);
}
