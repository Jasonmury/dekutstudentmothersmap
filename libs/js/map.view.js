$(document).ready(function(){
	function loadMap(){
	google.maps.visualRefresh = true;
	var mapOptions = {
	center: new google.maps.LatLng(mapCenter),
	zoom: 14,
	backgroundColor: 'khaki',
	mapTypeId: google.maps.MapTypeId.HYBRID,
	disableDefaultUI: true,
	zoomControl: true,
	zoomControlOptions: {
		style: google.maps.ZoomControlStyle.LARGE,
		position: google.maps.ControlPosition.LEFT_TOP
	},
	mapTypeControl: false,
	streetViewControl: true,
	streetViewControlOptions: {
		position: google.maps.ControlPosition.LEFT_TOP
	},
	overviewmapControl: true,
	overviewmapControlOptions: {
		opened: true
	}
	};

	var mapDiv = document.getElementById('map');
	map = new google.maps.Map(mapDiv, mapOptions);
	var centerMarker = new Marker({
	map: map,
	position: new google.maps.LatLng(mapCenter),
	icon: {
		path: MAP_PIN,
		fillColor: 'orange',
		fillOpacity: 0.6,
		strokeColor: 'purple',
		strokeWeight: 0.8
	},
	map_icon_label: '<span class="map-icon map-icon-university"></span>',
	draggable: false
	});
	google.maps.event.addListener(centerMarker, 'click', function(evt){
		var lat = evt.latLng.lat().toFixed(4);
		var lng = evt.latLng.lng().toFixed(4);
		var placeInfo = new google.maps.InfoWindow({
			content: '<div id="dekut"><span id="name">Dedan Kimathi University Of Technology.</span>'
			+'<span id="address">Location Address: B 5, Nyeri, Kenya. ('+mapCenter.lat+', '+mapCenter.lng+
			')</span><span id="Mobile Phone: ">+254713 835 965.</span>'+
			'<span id="web">Website: <a href="http://www.dkut.ac.ke/" target="blank" title="visit DeKUT Homepage.">www.dkut.ac.ke</a></span></div>'
		});
		placeInfo.open(map, centerMarker);
	});


	  var iconicMarker = new google.maps.Marker({
			position: new google.maps.LatLng(markerSeeker.lat, markerSeeker.lng),
			draggable:true,
			icon:'libs/img/marker-icon.png',
			label: {
				fontFamily: 'lucida console',
				fontStyle: 'italic',
				fontWeight: '2px',
				text: 'Position Seek',
				color: 'purple'
			},
			crossOnDrag: false,
			cursor: 'pointer',
			opacity: 0.6
			});
		iconicMarker.setMap(map);
		  google.maps.event.addListener(iconicMarker, 'click', function(evt){
			  var lat = evt.latLng.lat().toFixed(4);
			  var lng = evt.latLng.lng().toFixed(4);
				var placeInfo = new google.maps.InfoWindow({
					content: '<h3>Marker Position:<b>['+lat+', '+lng+']</b></h3>'
				});
				placeInfo.open(map, iconicMarker);
		});

		loadSavedRecords(map);
		var blueBaseMap = new google.maps.StyledMapType(blueStyle, {name: "Blue_Style_Map"});
		map.mapTypes.set('blue_baseMap', blueBaseMap);
		var night_mode = new google.maps.StyledMapType(nightStyle, {name: "Night_Mode_Style_Map"});
		map.mapTypes.set('nightMode', night_mode);
		var osmMap = new google.maps.ImageMapType({
			getTileUrl: function(coord, zoom) {
			return "http://tile.openstreetmap.org/" + zoom +
			"/" + coord.x + "/" + coord.y + ".png";
			},
			tileSize: new google.maps.Size(256, 256),
			name: "Open_Street_Map",
			maxZoom: 25,
			opacity: 0.7
		});
		map.mapTypes.set('OSM', osmMap);

	  var geoLocate = document.createElement('div');
	  var geoLocated = new geoLocator(geoLocate, map);
	  geoLocate.style['padding-top'] = '4px';
	  map.controls[google.maps.ControlPosition.TOP_LEFT].push(geoLocate);


		var advanced = document.createElement('div');
		var advancedTool = new advancedToolLayer(advanced, map);
		advanced.style['padding-top'] = '4px';
		map.controls[google.maps.ControlPosition.RIGHT_TOP].push(advanced);

	 var iconContainer = document.createElement('div');
	 var setBaseMaps = new addlayersIcon(iconContainer, map);
	 iconContainer.style['padding-top'] = '4px';
	 map.controls[google.maps.ControlPosition.RIGHT_TOP].push(iconContainer);


	  var coordBox = document.createElement('div');
	  var modBox = new getOverCooord(coordBox, map);
	  map.controls[google.maps.ControlPosition.BOTTOM_LEFT].push(coordBox);

		var searchInput = document.createElement('input');
		searchInput.id = 'search-box';
		searchInput.title = "Search services and products supported by the Application.";
		searchInput.placeholder = "search item....";
		var searchBox = new google.maps.places.SearchBox(searchInput);
		var searchControl = new searchBoxAdd(searchBox, map);
		 map.controls[google.maps.ControlPosition.TOP_LEFT].push(searchInput);
		 google.maps.event.trigger(window, "resize");
	  var drawingManager = new google.maps.drawing.DrawingManager({
		  drawingControl: true,
		  drawingControlOptions: {
			  position: google.maps.ControlPosition.TOP_RIGHT,
				drawingModes: ['marker', 'circle']   /*, 'polygon', 'polyline', 'rectangle'*/
		  },

		/*drawingMode: google.maps.drawing.OverlayType.MARKER,
		polylineOptions: {
		strokeColor: 'violet',
		strokeWeight: 3
		},
		polygonOptions: {
		strokeColor: 'green',
		strokeWeight: 2,
		fillColor: 'pink',
		fillOpacity: 0.2
	},
	rectangleOptions: {
	strokeColor: 'blue',
	strokeWeight: 2,
	fillColor: 'grey',
	fillOpacity: 0.3
	},
	*/
		markerOptions: {
			draggable: true,
			title: "Click To Register a Student Mother",
			icon: {
					path: MAP_PIN,
					fillColor: 'magenta',
					fillOpacity: 0.3,
					strokeColor: 'green',
					strokeWeight: 0.9
				},
			map_icon_label: '<span class="map-icon map-icon-zoom-in">Register.</span>',
			crossOnDrag: false
		},
		circleOptions: {
		strokeColor: 'blue',
		strokeWeight: 2,
		fillColor: 'yellow',
		fillOpacity: 0.2
		}
	  });
		drawingManager.setMap(map);

		google.maps.event.addListener(drawingManager, 'overlaycomplete', function(event){
		if (event.type == 'polyline') {
			event.overlay.setEditable(true);
			event.overlay.setDraggable(true);
			drawingManager.setDrawingMode(null);
			var latlng = event.overlay.getPath().getArray().toString();
			var getlength = event.overlay.getLength();
			console.log('Shape Line Length : '+getlength/1000+'Km.\nLatLng String : '+latlng);
		}
		else if(event.type == google.maps.drawing.OverlayType.POLYGON){
				event.overlay.setEditable(true);
				event.overlay.setDraggable(true);
				drawingManager.setDrawingMode(null);
				customizePolygon();
		}
		else if(event.type == google.maps.drawing.OverlayType.RECTANGLE){
				event.overlay.setEditable(true);
				event.overlay.setDraggable(true);
				drawingManager.setDrawingMode(null);
				customizeRectangle();
		}
		else if(event.type == google.maps.drawing.OverlayType.CIRCLE){
				event.overlay.setEditable(true);
				event.overlay.setDraggable(true);
				drawingManager.setDrawingMode(null);
				var radius = event.overlay.getRadius().toFixed(4);
				var bounds = event.overlay.getBounds();
				var center = event.overlay.getCenter();
				console.log('radius : '+radius+'mm. \nBounds : '+bounds+'\nCircle Center : '+center);
				google.maps.event.addListener(event.overlay, 'dragend', function(){
					drawingManager.setDrawingMode(null);
					center = event.overlay.getCenter();
					bounds = event.overlay.getBounds();
					console.log('New Bounds : '+bounds+'\nCircle Center : '+center);
				});
				google.maps.event.addListener(event.overlay, 'radius_changed', function(circle){
					radius = event.overlay.getRadius().toFixed(4);
					console.log('radius changed to : '+radius);
				});
		}
		else if(event.type == google.maps.drawing.OverlayType.MARKER){
			drawingManager.setDrawingMode(null);
			position = event.overlay.getPosition();
			console.log(position);
			google.maps.event.addListener(event.overlay, 'dragend', function(){
			position = event.overlay.getPosition();
			console.log(position.lat().toFixed(7), position.lng().toFixed(7));
			});
			console.log(position.lat().toFixed(7), position.lng().toFixed(7));
			google.maps.event.addListener(event.overlay, 'click', function(){
				addCustomMarker(position.lat().toFixed(5), position.lng().toFixed(5), map, event.overlay);
			});
		}
		});

		infoWindow = new google.maps.InfoWindow({
			content: document.getElementById('info-content')
		});

		var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer({
          draggable: true,
          map: map,
          panel: document.getElementById('dst-panel')
        });

        directionsDisplay.addListener('directions_changed', function() {
          computeTotalDistance(directionsDisplay.getDirections());
        });


	var startPoint = document.getElementById('start');
	var endPoint = document.getElementById('end');
	var wayPoint = document.getElementById('waypoints');


	var startSearch = new google.maps.places.SearchBox(startPoint);
	var endSearch = new google.maps.places.SearchBox(endPoint);
	var waySearch = new google.maps.places.SearchBox(wayPoint);
	waySearch.addListener('places_changed', function() {
	  var places = waySearch.getPlaces();
	  if (places.length == 0) {
		return;
	  }

	  var bounds = new google.maps.LatLngBounds();
	  places.forEach(function(place) {
		if (!place.geometry) {
		  console.log("Returned place contains no geometry");
		  return;
		}
		var placeLocation = {
		location: {lat:place.geometry.location.lat(), lng:place.geometry.location.lng()}
		};
		addWayPoints.push(placeLocation);
		displayNewRoute(addWayPoints, directionsService, directionsDisplay);
		var icon = {
		  url: place.icon,
		  size: new google.maps.Size(71, 71),
		  origin: new google.maps.Point(0, 0),
		  anchor: new google.maps.Point(17, 34),
		  scaledSize: new google.maps.Size(25, 25)
		};

		var placeMarker = new google.maps.Marker({
		  map: map,
		  icon: icon,
		  title: place.name,
		  position: place.geometry.location
		});
		var placeInfo = new google.maps.InfoWindow({
			content: '<h3><b>'+place.address_components[1].long_name+','+place.address_components[2].long_name+'</b></h3>'
		});
		google.maps.event.addListener(placeMarker, 'click', function(){
			placeInfo.open(map, placeMarker);
		});
		if (place.geometry.viewport) {
		  bounds.union(place.geometry.viewport);
		} else {
		  bounds.extend(place.geometry.location);
		}
	  });
	  map.fitBounds(bounds);
	});

 startSearch.addListener('places_changed', function() {
      var places =startSearch.getPlaces();
      if (places.length == 0) {
        return;
  }

  var bounds = new google.maps.LatLngBounds();
  places.forEach(function(place) {
    if (!place.geometry) {
      console.log("Returned place contains no geometry");
      return;
    }
	origin = new google.maps.LatLng(place.geometry.location.lat(),place.geometry.location.lng());
	var icon = {
	        url: place.icon,
	        size: new google.maps.Size(71, 71),
	        origin: new google.maps.Point(0, 0),
	        anchor: new google.maps.Point(17, 34),
	        scaledSize: new google.maps.Size(25, 25)
	      };

	      var placeMarker = new google.maps.Marker({
	        map: map,
	        icon: icon,
	        title: place.name,
	        position: place.geometry.location
	 });
	var placeInfo = new google.maps.InfoWindow({
		content: '<h3><b>'+place.address_components[1].long_name+','+place.address_components[2].long_name+'</b></h3>'
	});
	google.maps.event.addListener(placeMarker, 'click', function(){
		placeInfo.open(map, placeMarker);
	});
        if (place.geometry.viewport) {
          bounds.union(place.geometry.viewport);
        } else {
          bounds.extend(place.geometry.location);
        }
      });
      map.fitBounds(bounds);
    });


		 endSearch.addListener('places_changed', function() {
          var places =endSearch.getPlaces();
          if (places.length == 0) {
            return;
          }

          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
			destination = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
			displayMyRoute(directionsService, directionsDisplay);
			var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            var placeMarker = new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            });
			var placeInfo = new google.maps.InfoWindow({
				content: '<h3><b>'+place.address_components[1].long_name+','+place.address_components[2].long_name+'</b></h3>'
			});
			google.maps.event.addListener(placeMarker, 'click', function(){
				placeInfo.open(map, placeMarker);
			});
            if (place.geometry.viewport) {
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });



	  }

		google.maps.event.addDomListener(window, 'resize', function() {
			if($(document).outerWidth() <= 981){
				$('#floating-panel').hide();
			}else if($(document).outerWidth() > 981){
				$('#floating-panel').show();
			}
			if($(document).outerWidth() <= 1117){
			$('.autocompleteUI#autocompleteUI').hide();
		}else if($(document).outerWidth() > 1117){
			$('.autocompleteUI#autocompleteUI').show();
		}


		});
		google.maps.event.addDomListener(window, 'load', loadMap);
	});
