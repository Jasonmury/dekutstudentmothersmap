<div id="container" class="login modal-dialog container">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h1>LOGIN INTO <span>DeKUT SM</span> APPLICATION</h1>
	<form method="post" id="userLogin">
		<div class="form row">
		<input type="text" name="username" id="login-username" placeholder="Employee number or Email" />
		<label for="username">Employee Number or Email Address*</label>
		<input type="password" name="password" id="login-password" placeholder="password" />
		<label for="Pasword">Your Custom Password or National Identification Number*</label>
		<button class="submit"  id="admin-login">LOGIN</button>
		</div>
		<div class="clear"></div>
		<div id="login-note" class="row"><p class="text-warning">note: </p><span class="text-success bg-danger">Only <kbd>registered and authorised</kbd> personels can use this service.</span>
		<div id="status"></div>
		</div>
	</form>
</div>
