<div id="listing" >
  <table id="results-table">
  <button id="close-result">Hide contents</button>
  <tbody id="results"></tbody>
  </table>
</div>

<div id="dst-panel" class='hideDiv' >
  <p>Total Distance: <span id="total"></span></p>
</div>
<div class="container-fluid">
	<div class="row">
    <div class='hideDiv'>
      <div id="info-content">
        <table>
          <tr id="iw-url-row" class="iw_table_row">
            <td id="iw-icon" class="iw_table_icon"></td>
            <td id="iw-url"></td>
          </tr>
          <tr id="iw-address-row" class="iw_table_row">
            <td class="iw_attribute_name">Address:</td>
            <td id="iw-address"></td>
          </tr>
          <tr id="iw-phone-row" class="iw_table_row">
            <td class="iw_attribute_name">Telephone:</td>
            <td id="iw-phone"></td>
          </tr>
          <tr id="iw-rating-row" class="iw_table_row">
            <td class="iw_attribute_name">Rating:</td>
            <td id="iw-rating"></td>
          </tr>
          <tr id="iw-website-row" class="iw_table_row">
            <td class="iw_attribute_name">Website:</td>
            <td id="iw-website"></td>
          </tr>
        </table>
      </div>
    </div>

	 <div id="floating-panel">
    <b>Start: </b>
    <input type="text" placeholder="Origin Point" id='start' name='start-point' />
    <b>End: </b>
    <input type="text" placeholder="Destination Point" id='end' name='end-point' />
	<strong>Through: </strong>
	<input type="text" placeholder="Way Point(s)" id='waypoints' name='waypoints' />
	<b>Mode: </b>
    <select id="mode">
      <option value="DRIVING">Driving</option>
      <option value="WALKING">Walking</option>
      <option value="BICYCLING">Bicycling</option>
      <option value="TRANSIT">Transit</option>
    </select>

    </div>

	</div>
</div>
