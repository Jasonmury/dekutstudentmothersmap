<?php
$host = "127.0.0.1";
$user = "root";
$password = "";
$database = "cyber_smart";
$prepaid_tbl = "prepaid_clients";
$log_file = 'error_logs.file';
try{
	$server_connect = new PDO("mysql:host=$host; dbname=$database",$user,$password);
	$server_connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $err){
  $file = fopen($log_file, 'a+');
  fwrite($file, "\n".$err->getMessage());
  fclose($file);
	die("<div class='failed'>Server Entered Maintenence Mode ~ Service you are trying to request currently unsupported </div>");
}
 ?>
