<?php
class UserGenerator
{

	private $pass_min = 8;
	private $pass_max = 15;
	private $user_min = 7;
	private $user_max = 20;
	private $user_case = TRUE;
	private $salt = '$2a$07$usesomesillystringforsalt$';
	private $users = array();

	public function __construct($pass_min, $pass_max, $user_min, $user_max, $case_sensitive )
	{
		$this->pass_min = $pass_min;
		$this->pass_max = $pass_max;
		$this->user_min = $user_min;
		$this->user_case = $case_sensitive;
		$this->user_max = $user_max;
	}
	
	private function generate(){
			$username = $this->genUsername( $this->user_min, $this->user_max, $this->user_case );
			if (array_key_exists( $username, $this->users ) == false ){
				$password = $this->genPassword( $this->pass_min, $this->pass_max );
				$encrypted = $this->cryptPassword($password);
				$this->users[$username] = array('username' => $username,'password' => md5($encrypted) );
			   }

		return $this->users;
	}

	private function getQuery( $tablename, $columns)
	{
		$values = '';

		foreach ( $this->users as $u )
		{
			$values .= "(null,'{$u['username']}','{$u['encrypted']}'),";
		}
		return 'INSERT INTO '.$tablename.' ('.$columns.') VALUES'.rtrim($values, ",");

	}

	public function checkPassword ( $password )
	{
		// Query users password in DB. It will be stored encrypted, so use it here.
		$password_queried_from_db = '$2a$07$usesomesillystringfore2uDLvp1Ii2e./U9C8sBjqp8I90dH6hi'; // pass = rasmuslerdorf

		if (CRYPT_BLOWFISH == 1)
		{
	   	 if (crypt($password, $this->salt) == $password_queried_from_db)
	   	 {
				return true;
	   	 }
		}
		return false;

	}

	private function cryptPassword( $password )
	{
		return crypt($password, $this->salt);
	}


	private function genUsername($min, $max, $case_sensitive)
	{
		// Set length
		$length = rand($min, $max);

		// Set allowed chars (And whether they should use case)
		if ( $case_sensitive )
		{
			$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		}
		else
		{
			$chars = "abcdefghijklmnopqrstuvwxyz";
		}

		// Get string length
		$chars_length = strlen($chars);

		// Create username char for char
		$username = "";

		for ( $i = 0; $i < $length; $i++ )
		{
			$username .= $chars[mt_rand(0, $chars_length)];
		}

		return $username;

	}

	private function genPassword( $min, $max)
	{
		// Set length
		$length = rand($min, $max);

		// Set charachters to use
		$lower = 'abcdefghijklmnopqrstuvwxyz';
		$upper = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$chars = '123456789@#$%&';

		// Calculate string length
		$lower_length = strlen($lower);
		$upper_length = strlen($upper);
		$chars_length = strlen($chars);

		// Generate password char for char
		$password = '';
		$alt = time() % 2;
		for ($i = 0; $i < $length; $i++)
		{
			if ($alt == 0)
			{
				$password .= $lower[mt_rand(0, $lower_length)]; $alt = 1;
			}
			if ($alt == 1)
			{
				$password .= $upper[mt_rand(0, $upper_length)]; $alt = 2;
			}
			else
			{
				$password .= $chars[mt_rand(0, $chars_length)]; $alt = 0;
			}
		}
		return $password;
	}

	private function isNum( $num )
	{
		if ( is_int( (String) $num ) && ctype_digit((int) $num) && $num > 0 )
		{
			return true;
		}
		return false;
	}
}

 ?>
